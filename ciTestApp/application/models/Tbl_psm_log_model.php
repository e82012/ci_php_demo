<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_psm_log_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // 取得
    public function get_all_data() {
        $query = $this->db->get('tbl_psm_log');
        return $query->result();
    }
    // 新增
    public function insert_data($data) {
        $this->db->insert('tbl_psm_log', $data);
    }

    // 更新
    public function update_data($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tbl_psm_log', $data);
    }

    // 刪除
    public function delete_data($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_psm_log');
    }
    // 依照年月取得資料
    public function get_data_by_ym($y,$m){
        $sort = $this->area_sort();
        $query = $this->db->order_by("FIELD(area,$sort)")
                            ->get_where('tbl_psm_log', array('date_year' => $y,'date_month'=> $m));
                            
        return $query->result();
    }
    // Sort
    public function area_sort(){
        $area_sort = "
            '北一區',
            '北二區',
            '北三區',
            '北四區',
            '北五區',
            '竹苗區',
            '桃一區',
            '中一區',
            '中二區',
            '中三區',
            '嘉南一區',
            '嘉南二區',
            '高屏一區',
            '高屏二區',
            '高屏三區',
            '高屏四區',
            '高屏五區',
            '高屏六區'
            ";
        return $area_sort;
    }

}