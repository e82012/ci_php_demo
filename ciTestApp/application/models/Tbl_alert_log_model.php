<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_alert_log_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // 取得所有log
    public function get_all_log() {
        $query = $this->db->get('tbl_alert_log');
        return $query->result();
    }

    // 依照ID 取得log
    public function get_log_by_id($id) {
        $query = $this->db->get_where('tbl_alert_log', array('id' => $id));
        return $query->row();
    }

    // 新增log
    public function insert_log($data) {
        $this->db->insert('tbl_alert_log', $data);
    }

    // 更新log
    public function update_log($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tbl_alert_log', $data);
    }

    // 刪除log
    public function delete_log($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_alert_log');
    }


}