<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_goods_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // 取得所有產品
    public function get_all_goods() {
        $query = $this->db->get('tbl_goods');
        return $query->result();
    }

    // 依照產品 ID 取得產品資料
    public function get_goods_by_id($id) {
        $query = $this->db->get_where('tbl_goods', array('id' => $id));
        return $query->row();
    }

    // 依照產品 goods_id 取得客戶資料
    public function get_goods_by_goods_id($customerId) {
        $query = $this->db->get_where('tbl_goods', array('goods_id' => $customerId));
        return $query->result();
    }
      // 依照產品 goods_name 取得客戶資料
      public function get_goods_by_goods_name($customerName) {
        $query = $this->db->get_where('tbl_goods', array('goods_name' => $customerName));
        return $query->result();
    }
    // 新增產品資料
    public function insert_goods($data) {
        $this->db->insert('tbl_goods', $data);
    }

    // 更新產品資料
    public function update_goods($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tbl_goods', $data);
    }

    // 刪除產品資料
    public function delete_goods($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_goods');
    }


}