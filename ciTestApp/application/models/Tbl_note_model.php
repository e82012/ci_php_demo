<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_note_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // 依照使用者 acc 取得使用者資料
    public function get_note_by_user_acc($acc) {
        $query = $this->db->get_where('tbl_note', array('note_user_acc' => $acc,'opt1'=>'1'));
        return $query->result();
    }
    // 新增note
    public function insert_note($data) {
        $this->db->insert('tbl_note', $data);
    }

    // 更新note
    public function update_note($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tbl_note', $data);
    }
    // 完成note
    public function update_note_completed($id){
        $udate = date("Y-m-d h:m:s");
        $data = array(
            'note_status'=>'1',
            'note_udate'=>$udate,
        );
        $this->db->where('id', $id);
        $this->db->update('tbl_note', $data);

    }
    // 刪除note
    public function delete_note($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_note');
    }
    // 取得需要發送通知的提醒事項
    public function _get_all_alert_note(){
        $conditions = array(
            'note_alert_date !=' => '0000-00-00 00:00:00',
            'note_alert_opt' => 0,
            'note_status' => 0,
            '(note_email_opt = 1 OR note_line_opt = 1)',
        );
        $query = $this->db->get_where('tbl_note',$conditions);
        return $query->result();
    }


}