<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_user_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    // 取得所有使用者
    public function get_all_user() {
        $query = $this->db->get('tbl_user');
        return $query->result();
    }

    // 依照使用者 ID 取得使用者資料
    public function get_user_by_id($id) {
        $query = $this->db->get_where('tbl_user', array('id' => $id));
        return $query->row();
    }

    // 依照使用者 user_id 取得使用者資料
    public function get_user_by_user_acc($user_acc) {
        $query = $this->db->get_where('tbl_user', array('user_acc' => $user_acc));
        return $query->result();
    }
      // 依照使用者user_id 取得 user_email 
      public function get_email_by_user_acc($user_acc) {
        $this->db->select('user_email');
        $query = $this->db->get_where('tbl_user', array('user_acc' => $user_acc));
        if ($query->num_rows() > 0) {
            return $query->row()->user_email;
        } else {
            return 0;
        }
    }
    // 新增使用者資料
    public function insert_user($data) {
        $this->db->insert('tbl_user', $data);
    }

    // 更新使用者資料
    public function update_user($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('tbl_user', $data);
    }

    // 刪除使用者資料
    public function delete_user($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_user');
    }


}