<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>CI_Framework</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
		.fadeinDiv
		{
			opacity:0;
		}
		.card-img-top {
			height: 250px; 
			object-fit: cover; 
		}

	</style>
</head>
<body>
	<!-- carousel 輪播圖-->
	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
			<img src="/image/carousel_1.jpg" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
			<img src="/image/carousel_2.jpg" class="d-block w-100" alt="...">
			</div>
		</div>
		<button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</button>
		<button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</button>
	</div>
	<p>
	<!-- 3 row carousel-->
	<div class="fadeinDiv">
		<div id="row_carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<!-- 3個row 每個row 4個items -->
				<?php for ($j = 0; $j < 3; $j++): ?>
					<div class="carousel-item <?php echo ($j == 0) ? 'active' : ''; ?>">
						<div class="row">
							<!-- 內容 -->
							<?php for ($i = $j * 4; $i < ($j + 1) * 4; $i++): ?>
								<!-- Card -->
								<div class="col-sm-3">
									<div class="card">
										<img src="<?php echo '/image/goods/'.$all_goods[$i]->goods_img ?>" class="btn card-img-top" data-toggle="modal" data-target="<?php echo '#'.$all_goods[$i]->goods_id ?>">
										<div class="card-body">
											<h5 class="card-title"><?php echo $all_goods[$i]->goods_name ?></h5>
											<p class="card-text"><small class="text-muted">NT$ <?php echo number_format($all_goods[$i]->goods_price) ?> </small></p>
										</div>
									</div>
								</div>
								<!-- Modal -->
								<div class="modal fade" id="<?php echo $all_goods[$i]->goods_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel"><?php echo $all_goods[$i]->goods_name ?></h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<img src="<?php echo '/image/goods/'.$all_goods[$i]->goods_img ?>" class="figure-img img-fluid rounded" data-toggle="modal" data-target="<?php echo '#'.$all_goods[$i]->goods_id ?>">
											<h5 class="card-title"><?php echo $all_goods[$i]->goods_name ?></h5>
											<p class="card-text"><?php echo $all_goods[$i]->goods_info ?> </p>
											<p class="card-text"><small class="text-muted">NT$ <?php echo number_format($all_goods[$i]->goods_price) ?> </small></p>

										</div>
										</div>
									</div>
								</div>
							<?php endfor ?>
						</div>
					</div>
				<?php endfor ?>
			</div>
			<button class="carousel-control-prev" type="button" data-target="#row_carousel" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</button>
			<button class="carousel-control-next" type="button" data-target="#row_carousel" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</button>
		</div>
	</div>
	<p>
	<!-- fade in 使用者提醒事項 -->
	<div class="fadeinDiv">
		<div class="card">
			<div class="card-header">
				個人提醒事項
			</div>
			<div class="card-body">
				<blockquote class="blockquote mb-0">
				<?php if(isset($seesionData['user_acc'])):?>
					您好 <?php echo $seesionData['user_acc'] . $seesionData['user_name']; ?>
					<p>
					<!-- tab 標籤 -->
					<ul class="nav nav-tabs" id="noteTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link" id="completed_tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="true">已完成</a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" id="uncompleted_tab" data-toggle="tab" href="#uncompleted" role="tab" aria-controls="uncompleted" aria-selected="false">未完成</a>
						</li>
						<li class="nav-item ml-auto">
							<a class="btn btn-sm btn-outline-success" id="new_note" data-toggle="modal" data-target="#new_note_modal">建立提醒事項</a>
						</li>
					</ul>
					<!-- 建立提醒事項modal -->
					<div class="modal fade" id="new_note_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="new_note_modal_Label">建立提醒事項</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
							<form>
								<div class="mb-3">
									<label for="note_title" class="form-label">標題：</label>
									<input type="text" class="form-control" id="note_title" name="note_title">
								</div>

								<div class="mb-3 form-check form-switch">
									<input class="form-check-input" type="checkbox" id="note_email_opt" name="note_email_opt">
									<label class="form-check-label" for="note_email_opt">啟用 Email提醒通知</label>
								</div>

								<div class="mb-3 form-check form-switch">
									<input class="form-check-input" type="checkbox" id="note_line_opt" name="note_line_opt">
									<label class="form-check-label" for="note_line_opt">啟用 Line提醒通知</label>
								</div>

								<div class="mb-3">
									<label for="note_line_token" class="form-label">Line Token：</label>
									<input type="text" class="form-control" id="note_line_token" name="note_line_token">
								</div>

								<div class="mb-3">
									<label for="note_alert_date" class="form-label">提醒時間設定：</label>
									<input type="datetime-local" class="form-control" id="note_alert_date" name="note_alert_date">
								</div>

								<div class="mb-3">
									<label for="note_content" class="form-label">內容：</label>
									<textarea class="form-control" id="note_content" name="note_content" rows="4"></textarea>
								</div>
							</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="create_new_note" onclick="create_note_js()">新增</button>
							</div>
							</div>
						</div>
					</div>
					<!-- tab note 內容 -->
					<div class="tab-content" id="noteContent">
						<!-- tab note 已完成 -->
						<div class="tab-pane fade" id="completed" role="tabpanel" aria-labelledby="completed_tab">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>標題</th>
										<th>建立日期</th>
										<th>完成日期</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach($note_ary as $note)
										{
											if($note->note_status == 1){
												echo "<tr data-toggle='modal' data-target='#note_modal_".$note->id."'>";
													echo "<td>".$note->note_title."</td>";
													echo "<td>".$note->note_cdate."</td>";
													echo "<td>".$note->note_udate."</td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
						<!-- tab note 未完成 -->
						<div class="tab-pane fade show active" id="uncompleted" role="tabpanel" aria-labelledby="uncompleted_tab">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>標題</th>
										<th>建立日期</th>
										<th>提醒時間</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										foreach($note_ary as $note)
										{
											if($note->note_status == 0){
												echo "<tr data-toggle='modal' data-target='#note_modal_".$note->id."'>";
													echo "<td>".$note->note_title."</td>";
													echo "<td>".$note->note_cdate."</td>";
													echo "<td>".$note->note_alert_date."</td>";
												echo "</tr>";
											}
										}
									?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- tab note modal 資料內容 -->
					<?php foreach($note_ary as $note):?>
						<!-- 資料veiw -->
						<div class="modal fade" id="note_modal_<?php echo $note->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
								<div class="modal-header" style="display: flex; flex-direction: column; align-items: flex-start;">
									<h5 class="modal-title" id="note_modal_<?php echo $note->id?>_Label"><?php echo $note->note_title ?></h5>
									<small class="text-muted" style="font-size: 14px">建立時間:<?php echo $note->note_cdate?></small>
									<!-- 如果有修改時間再秀 -->
									<?php if($note->note_udate != ''):?>
										<small class="text-muted" style="font-size: 14px">修改時間:<?php echo $note->note_udate?></small>
									<?php endif ?>

								</div>
								<div class="modal-body">
									<p class="card-text"><strong>Email提醒通知：</strong><?php echo $note->note_email_opt == 1 ? '啟用' : '未啟用' ?></p>
									<p class="card-text"><strong>Line提醒通知：</strong><?php echo $note->note_line_opt == 1 ? '啟用' : '未啟用' ?></p>
									<p class="card-text"><strong>Line Token：</strong><?php echo $note->note_line_token ?></p>
									<p class="card-text"><strong>提醒時間設定：</strong><?php echo $note->note_alert_date ?></p>
									<p class="card-text"><strong>提醒通知：</strong><?php echo $note->note_alert_opt == 1 ? '已發送':'未發送' ?></p>
									<p class="card-text"><strong>內容：</strong><?php echo $note->note_content ?></p>
								</div>
								<!-- 狀態未完成，才可修改予刪除 -->
								<?php if($note->note_status == 0):?>
									<div class="modal-footer text-center">
										<div class="btn-group" role="group" aria-label="Node Actions">
										<button type="button" class="btn btn-success" onclick="completed_node_js(<?php echo $note->id?>)">已完成</button>
											<button type="button" class="btn btn-warning" data-toggle='modal' data-target='#u_note_modal_<?php echo $note->id?>' onclick="$('#note_modal_<?php echo $note->id?>').modal('hide')">修改 </button>
											<button type="button" class="btn btn-danger" onclick="delete_node_js(<?php echo $note->id?>)">刪除</button>
										</div>
									</div>
								<?php endif ?>
							</div>
							</div>
						</div>
						<!-- 修改view -->
						<?php if($note->note_status == 0): ?>
							<div class="modal fade" id="u_note_modal_<?php echo $note->id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
									<div class="modal-header" style="display: flex; flex-direction: column; align-items: flex-start;">
										<h5 class="modal-title" id="note_modal_<?php echo $note->id?>_Label">提醒事項修改</h5>
										<small class="text-muted" style="font-size: 14px">建立時間:<?php echo $note->note_cdate?></small>
										<!-- 如果有修改時間再秀 -->
										<?php if($note->note_udate != ''):?>
											<small class="text-muted" style="font-size: 14px">修改時間:<?php echo $note->note_udate?></small>
										<?php endif ?>
									</div>
									<div class="modal-body">
										<form>
											<div class="mb-3">
												<label for="u_note_title_<?php echo $note->id?>" class="form-label">標題：</label>
												<input type="text" class="form-control" id="u_note_title_<?php echo $note->id?>" name="note_title" value = "<?php echo $note->note_title?>">
											</div>
											<div class="mb-3 form-check form-switch">
												<input class="form-check-input" type="checkbox" id="u_note_email_opt_<?php echo $note->id ?>" name="note_email_opt" <?php echo $note->note_email_opt == 1 ? 'checked' : ''; ?>>
												<label class="form-check-label" for="u_note_email_opt_<?php echo $note->id?>">啟用 Email提醒通知</label>
											</div>

											<div class="mb-3 form-check form-switch">
												<input class="form-check-input" type="checkbox" id="u_note_line_opt_<?php echo $note->id ?>" name="note_line_opt" <?php echo $note->note_line_opt == 1 ? 'checked' : ''; ?>>
												<label class="form-check-label" for="u_note_line_opt_<?php echo $note->id?>">啟用 Line提醒通知</label>
											</div>

											<div class="mb-3">
												<label for="u_note_line_token_<?php echo $note->id?>" class="form-label">Line Token：</label>
												<input type="text" class="form-control" id="u_note_line_token_<?php echo $note->id?>" name="note_line_token" value = "<?php echo $note->note_line_token?>">
											</div>

											<div class="mb-3">
												<label for="u_note_alert_date_<?php echo $note->id?>" class="form-label">提醒時間設定：</label>
												<input type="datetime-local" class="form-control" id="u_note_alert_date_<?php echo $note->id?>" name="note_alert_date" value = "<?php echo $note->note_alert_date?>">
											</div>

											<div class="mb-3">
												<label for="u_note_content_<?php echo $note->id?>" class="form-label">內容：</label>
												<textarea class="form-control" id="u_note_content_<?php echo $note->id?>" name="note_content" rows="4"><?php echo $note->note_content?></textarea>
											</div>
										</form>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-warning" onclick="update_node_js(<?php echo $note->id?>)">確定修改</button>
									</div>
								</div>
							</div>
						</div>
						<?php endif ?>
					<?php endforeach ?>
				<?php else :?>
					尚未登入，無法使用此功能
					<footer class='blockquote-footer' data-toggle='modal' data-target='#login_modal'>按此登入</footer>
				<?php endif?>
				</blockquote>
			</div>
		</div>
	</div>
	<p>
	<!-- <div style="position: absolute; top: 60px; right: 0;">
		<div class="toast show" role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
				<strong class="mr-auto">CI Test</strong>
				<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" onclick = "$('.toast').toast('hide');">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="toast-body">
				<p class="card-text">PHP Version: <?php echo phpversion(); ?></p>
				<p class="card-text">CodeIgniter Version: <?php echo CI_VERSION; ?></p>
				<p class="card-text">Page rendered in <strong>{elapsed_time}</strong> seconds.   </p>
			</div>
		</div>
	</div> -->
	<!-- 測試用按鈕 -->
	<div class="fixed-bottom">
		<button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#test_modal">
			_test_js
		</button>
	</div>
	<!-- 測試用modal -->
	<div class="modal fade" id="test_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">_test_js</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5 class="card-title"></h5>
					<form>
						<p class="card-text"><small class="text-muted">SheetJS</small></p>
						<!-- SheetJs 上傳 -->
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="import_type" id="radioTest" value="test" checked>
							<label class="form-check-label" for="radioTest">測試</label>
						</div>
						<div class="form-check form-check-inline">
							<input class="form-check-input" type="radio" name="import_type" id="radioPSM" value="psm">
							<label class="form-check-label" for="radioPSM">PSM資料上傳</label>
						</div>
						<div class="input-group mb-3">
							<div class="custom-file">
								<input type="file" class="custom-file-input" id="excelFile" accept=".xlsx, .xls" onchange="display_file_name(this)">
								<label class="custom-file-label" for="excelFile">選擇檔案</label>
							</div>
							<div class="input-group-append">
								<button type="button" class="btn btn-sm btn-outline-secondary" onclick="upload_excel()">上傳</button>
							</div>
						</div>
						<hr>
						<!--  -->
						<p class="card-text"><small class="text-muted">OPEN AI</small></p>
						<div class="mb-3">
							<input type="text" class="form-control form-control-sm" id="openai_apiKey" placeholder="API Key">
						</div>
						<div class="mb-3">
							<input type="text" class="form-control form-control-sm" id="openai_question" placeholder="輸入你的問題">
						</div>
						<div class="d-flex justify-content-end">
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="openAI_ask()">送出</button>
						</div>
						<hr>
						<!--  -->
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
$(document).ready(function() {
	// 滾輪滑動div fadein效果 hideme
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.fadeinDiv').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},500);
                    
            }
            
        }); 
    
    });
});
// 建立提醒事項
function create_note_js(){
	// 內容
	const note_title = document.getElementById('note_title').value;
	const note_email_opt = document.getElementById('note_email_opt').checked?'1':'0';
	const note_line_opt = document.getElementById('note_line_opt').checked?'1':'0';
	const note_line_token = document.getElementById('note_line_token').value;
	const note_alert_date = document.getElementById('note_alert_date').value;
	const note_content = document.getElementById('note_content').value;
	// 建立 確認有無填寫標題與內容
	if(note_title != '' && note_content != ''){
		// 執行
		$.ajax({
          url: 'index.php/Welcome/create_note_js', 
          method: 'POST',
          dataType:'JSON',
          data: {
            note_title:note_title,
            note_email_opt:note_email_opt,
            note_line_opt:note_line_opt,
            note_line_token:note_line_token,
			note_alert_date:note_alert_date,
			note_content:note_content,
			session_user_acc:sessionStorage['user_acc']
          }, 
          success: function(response) {
			console.log(response)
            switch(response[0]){
              case '0' :
                  Swal.fire('新增失敗', response[1], 'error');
                break;
              case '1' :
                // 先關閉modal
                $('#new_note_modal').modal('hide')
                // Alert
                Swal.fire({
                    title: '新增成功',
                    text: note_title,
                    icon: 'success',
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    didClose: () => {
                      // 重新載入
                      window.location.reload();
                    }
                  });
                break;
            }
          },
          error: function(xhr, status, error) {
            // 處理 AJAX 失敗的回應
            console.error(xhr);
            Swal.fire('失敗', '', 'error');
          }
        });
	}
	else
	{
		Swal.fire('請填寫完整資料','','error');
	}


}
// 修改提醒事項
function update_node_js(id){
	const u_modal = '#u_note_modal_'+id;
	// 內容
	const note_id = id;
	const note_title = document.getElementById('u_note_title_'+id).value;
	const note_email_opt = document.getElementById('u_note_email_opt_'+id).checked?'1':'0';
	const note_line_opt = document.getElementById('u_note_line_opt_'+id).checked?'1':'0';
	const note_line_token = document.getElementById('u_note_line_token_'+id).value;
	const note_alert_date = document.getElementById('u_note_alert_date_'+id).value;
	const note_content = document.getElementById('u_note_content_'+id).value;
	console.log(note_title)
	// 更新 確認有無填寫標題與內容
	if(note_id!= ''&& note_title != '' && note_content != ''){
		// 執行
		Swal.fire({
			title: "確定修改嗎?",
			showCancelButton: true,
			confirmButtonText: "確定",
			cancelButtonText: "取消",
			allowOutsideClick: false,
			}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'index.php/Welcome/update_note_js', 
					method: 'POST',
					dataType:'JSON',
					data: {
						note_id:note_id,
						note_title:note_title,
						note_email_opt:note_email_opt,
						note_line_opt:note_line_opt,
						note_line_token:note_line_token,
						note_alert_date:note_alert_date,
						note_content:note_content,
						session_user_acc:sessionStorage['user_acc']
					}, 
					success: function(response) {
						switch(response[0]){
						case '0' :
							Swal.fire('更新失敗', response[1], 'error');
							break;
						case '1' :
							// 先關閉modal
							$(u_modal).modal('hide')
							// Alert
							Swal.fire({
								title: '更新成功',
								text: note_title,
								icon: 'success',
								toast: true,
								position: 'center',
								showConfirmButton: false,
								timer: 2000,
								timerProgressBar: true,
								allowOutsideClick: false,
								allowEscapeKey: false,
								didClose: () => {
								// 重新載入
								window.location.reload();
								}
							});
							break;
						}
					},
					error: function(xhr, status, error) {
							// 處理 AJAX 失敗的回應
							console.error(xhr);
							Swal.fire('失敗', '', 'error');
						}
				});
			}});
	}
	else
	{
		Swal.fire('請填寫完整資料','','error');
	}
}
// 完成提醒事項
function completed_node_js(id){
	const v_modal = '#note_modal_'+id;
	// 內容
	const note_id = id;
	const note_title = document.getElementById('u_note_title_'+id).value;
	const note_content = document.getElementById('u_note_content_'+id).value;
	// 更新 確認有無填寫標題與內容
	if(note_id!= ''&& note_title != '' && note_content != ''){
		// 執行
		Swal.fire({
			title: "確定完成項目嗎?",
			showCancelButton: true,
			confirmButtonText: "確定",
			cancelButtonText: "取消",
			allowOutsideClick: false,
			}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'index.php/Welcome/completed_note_js', 
					method: 'POST',
					dataType:'JSON',
					data: {
						note_id:note_id,
						note_title:note_title,
						note_content:note_content,
						session_user_acc:sessionStorage['user_acc']
					}, 
					success: function(response) {
						switch(response[0]){
						case '0' :
							Swal.fire('更新失敗', response[1], 'error');
							break;
						case '1' :
							// 先關閉modal
							$(v_modal).modal('hide')
							// Alert
							Swal.fire({
								title: '提醒事項完成!',
								text: note_title,
								icon: 'success',
								toast: true,
								position: 'center',
								showConfirmButton: false,
								timer: 2000,
								timerProgressBar: true,
								allowOutsideClick: false,
								allowEscapeKey: false,
								didClose: () => {
								// 重新載入
								window.location.reload();
								}
							});
							break;
						}
					},
					error: function(xhr, status, error) {
							// 處理 AJAX 失敗的回應
							console.error(xhr);
							Swal.fire('失敗', '', 'error');
						}
				});
			}});
	}
	else
	{
		Swal.fire('失敗','','error');
	}
}
// 刪除提醒事項
function delete_node_js(id){
	const v_modal = '#note_modal_'+id;
	// 內容
	const note_id = id;
	const note_title = document.getElementById('u_note_title_'+id).value;
	const note_content = document.getElementById('u_note_content_'+id).value;
	// 更新 確認有無填寫標題與內容
	if(note_id!= ''&& note_title != '' && note_content != ''){
		// 執行
		Swal.fire({
			title: "確定刪除項目嗎?",
			showCancelButton: true,
			confirmButtonText: "確定",
			cancelButtonText: "取消",
			allowOutsideClick: false,
			}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: 'index.php/Welcome/delete_note_js', 
					method: 'POST',
					dataType:'JSON',
					data: {
						note_id:note_id,
						note_title:note_title,
						note_content:note_content,
						session_user_acc:sessionStorage['user_acc']
					}, 
					success: function(response) {
						switch(response[0]){
						case '0' :
							Swal.fire('刪除失敗', response[1], 'error');
							break;
						case '1' :
							// 先關閉modal
							$(v_modal).modal('hide')
							// Alert
							Swal.fire({
								title: '提醒事項刪除!',
								text: note_title,
								icon: 'success',
								toast: true,
								position: 'center',
								showConfirmButton: false,
								timer: 2000,
								timerProgressBar: true,
								allowOutsideClick: false,
								allowEscapeKey: false,
								didClose: () => {
								// 重新載入
								window.location.reload();
								}
							});
							break;
						}
					},
					error: function(xhr, status, error) {
							// 處理 AJAX 失敗的回應
							console.error(xhr);
							Swal.fire('失敗', '', 'error');
						}
				});
			}});
	}
	else
	{
		Swal.fire('失敗','','error');
	}
}
// SheetJS 上傳檔案
function upload_excel(){
	// 先出現處理畫面
	Swal.fire({
		title: '資料讀取中...',
		allowOutsideClick: false,
	});
	const input = document.getElementById('excelFile');
	// import_type
	const import_type = document.querySelector('input[name="import_type"]:checked').value;
    
    const file = input.files[0];
	// 如果有檔案
    if (file) {
        const reader = new FileReader();
        reader.onload = function (e) {
            const data = e.target.result;
            const workbook = XLSX.read(data, { type: 'binary' });
            // 選擇第一個工作表
            const worksheet = workbook.Sheets[workbook.SheetNames[0]];
            // 將工作表轉換為 JSON
            const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
            // 進行處理
			switch(import_type){
				case 'test':
					console.log(jsonData);
					const data_length = jsonData.length;
					Swal.fire('讀取成功', '資料長度 : ' + data_length , 'success');
					break;
				case 'psm':
					Swal.fire({
					title: '請稍候...',
					text: 'PSM資料正在處理中',
					// allowOutsideClick: false,
					showConfirmButton: false,
					didOpen:()=>{
						Swal.showLoading();
						// 執行 因為資料量太大所以切分
							let data_count = 0; //切分用
							let total_data = 0; //共計筆數
							let set_data_ary = []; //寫入用
							jsonData.forEach(function(data,index){
								set_data_ary.push(data);
								data_count ++;
								total_data ++;
								if(data_count >= 30 || index == jsonData.length - 1){
									// AJAX
									$.ajax({
									url: 'index.php/Psm/import_excel_sheetJs', 
									method: 'POST',
									dataType:'JSON',
									data:{
										session_user_acc:sessionStorage['user_acc'],
										jsonData:set_data_ary,
									},
									success: function(response) {
										// 回應
										// console.log("AJAX")
										// console.log(response)
									},
									error: function(xhr, status, error) {
											// 處理 AJAX 失敗的回應
											console.error(xhr);
											Swal.fire('失敗', '', 'error');
										}
									});
									// 清空
									data_count = 0;
									set_data_ary = [];
								}
							})
							// 執行結束
							Swal.fire('上傳成功', '共計 ' + total_data + ' 筆資料', 'success');
						}
						
					});
					break;
			}
        };
        reader.readAsBinaryString(file);
    }
	else
	{
		console.log("讀取失敗!");
		Swal.fire('失敗', '尚未選擇檔案', 'error');
	}
}
// SheetJS 預覽選擇檔案
function display_file_name(input){
		const file_Name = input.files[0].name;
        const label = input.nextElementSibling;
        label.innerText = file_Name;
}
// CHAT_GPT
function openAI_ask(){
	const api_key = document.getElementById('openai_apiKey').value;
	const ask_question = document.getElementById('openai_question').value;
	console.log(api_key+"_"+ask_question);
	Swal.fire({
        title: '請稍候...',
        text: 'OPEN AI 正在處理中',
        // allowOutsideClick: false,
        showConfirmButton: false,
		didOpen:()=>{
			Swal.showLoading();
			if(api_key != '' && ask_question != ''){
				// 執行
				$.ajax({
					url: 'https://api.openai.com/v1/chat/completions', 
					method: 'POST',
					dataType:'JSON',
					headers: {
						"Authorization": "Bearer "+api_key,
						"Content-Type": "application/json"
						},
					data: JSON.stringify({
						model: "gpt-3.5-turbo",
						messages: [
							{ role: "user", content: ask_question }
						]
					}),
					success: function(response) {
						// 回應
						console.log(response)
					},
					error: function(xhr, status, error) {
							// 處理 AJAX 失敗的回應
							console.error(xhr);
							Swal.fire('失敗', '', 'error');
						}
				});
			}
			else
				Swal.fire('失敗', '未輸入資料', 'error');

		}
   
    });
}
</script>