<!DOCTYPE html>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>

<!-- BS4 -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>

<!-- SweetAlert2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- ckeditor -->
<script src="https://cdn.ckeditor.com/4.19.0/standard/ckeditor.js"></script>
<!-- <script src="/vendor/responsive_filemanager/filemanager/plugin.min.js"></script> -->

<!-- SheetJs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.9/xlsx.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.5/FileSaver.min.js"></script>

<div class="fixed-top">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/..">CI Framework Test</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <!-- <li class="nav-item">
        <a class="nav-link" href="/../index.php/customer">Customer</a>
      </li> -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="index.php/psm">PSM</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <?php if(isset($seesionData['user_acc'])):?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
              <?php echo $seesionData['user_acc'] ?>
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#" onclick="logout_Js()">登出</a>
            </div>
          </li>
        <?php else: ?>
          <a class="nav-link btn" id = "nav_login" data-toggle="modal" data-target="#login_modal">登入</a>
        <?php endif ?>
      </li>
    </ul>
  </div>
</nav>
</div>
<!-- 登入 Modal -->
<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="login_modal_Label">會員系統</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
        <div class="mb-3">
          <label for="user_acc" class="form-label">帳號：</label>
          <input type="text" class="form-control" id="user_acc" name="user_acc">
        </div>
        <div class="mb-3">
          <label for="user_pwd" class="form-label">密碼：</label>
          <input type="password" class="form-control" id="user_pwd" name="user_pwd">
        </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" id="register_Btn" data-toggle="modal" data-target="#register_modal" onclick="$('#login_modal').modal('hide')">註冊會員</button>
        <button type="button" class="btn btn-primary" id="login_Btn" onclick="login_Js()">登入</button>
      </div>
    </div>
  </div>
</div>
<!-- 註冊 Modal -->
<div class="modal fade" id="register_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="register_modal_Label">會員系統</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
        <div class="mb-3">
          <label for="r_user_acc" class="form-label">帳號：</label>
          <input type="text" class="form-control" id="r_user_acc" name="r_user_acc">
        </div>
        <div class="mb-3">
          <label for="r_user_pwd" class="form-label">密碼：</label>
          <input type="password" class="form-control" id="r_user_pwd" name="r_user_pwd">
        </div>
        <div class="mb-3">
          <label for="r_user_name" class="form-label">名稱：</label>
          <input type="text" class="form-control" id="r_user_name" name="r_user_name">
        </div>
        <div class="mb-3">
          <label for="r_user_email" class="form-label">email：</label>
          <input type="email" class="form-control" id="r_user_email" name="r_user_email">
        </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" id="register_submit" onclick="register_Js()">註冊會員</button>
      </div>
    </div>
  </div>
</div>
<!-- 留個空白 -->
<br>
<script>
  // js登入
  function login_Js(){
    const user_acc = document.getElementById("user_acc").value;
    const user_pwd = document.getElementById("user_pwd").value;
    // AJAX
    $.ajax({
				url: 'index.php/User/login_Js', 
				method: 'POST',
				dataType:'JSON',
				data: {
					user_acc:user_acc,
          user_pwd:user_pwd
				}, 
				success: function(response) {
          console.log(response[0]);
          switch(response[0]){
            case '0' :
                Swal.fire('登入失敗', response[1], 'error');
              break;
            case '1' :
                // 先關閉登入modal
                $('#login_modal').modal('hide')
                Swal.fire({
                  title: '登入成功',
                  text: response[1],
                  icon: 'success',
                  toast: true,
                  position: 'center',
                  showConfirmButton: false,
                  timer: 1000,
                  timerProgressBar: true,
                  allowOutsideClick: false,
                  allowEscapeKey: false,
                  didClose: () => {
                    // 重新載入
                    window.location.reload();
                    // seesion
                    sessionStorage.setItem("user_acc", response[1]);
                  }
                });
              break;
          }
        },
				error: function(xhr, status, error) {
					// 處理 AJAX 失敗的回應
					console.error(xhr);
					Swal.fire('失敗', '', 'error');
				}
			});
  }
  // js登出
  function logout_Js(){
    Swal.fire({
      title: "確定登出嗎?",
      showCancelButton: true,
      confirmButtonText: "確定",
      cancelButtonText: "取消",
      allowOutsideClick: false,
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          url: 'index.php/User/logout_js', 
          method: 'POST',
          dataType:'JSON',
          data: {
          }, 
          success: function(response) {
            console.log(response);
            // 清除
            sessionStorage.clear();
            // 重新載入
            window.location.reload();
          },
          error: function(xhr, status, error) {
            // 處理 AJAX 失敗的回應
            console.error(xhr);
            Swal.fire('失敗', '', 'error');
          }
        });
      }
    });
  }
  // js註冊
  function register_Js(){
    const r_user_acc = document.getElementById('r_user_acc').value;
    const r_user_pwd = document.getElementById('r_user_pwd').value;
    const r_user_name = document.getElementById('r_user_name').value;
    const r_user_email = document.getElementById('r_user_email').value;
    // 都有填寫才能進行註冊
    if(r_user_acc != '' && r_user_pwd != '' && r_user_name != '' && r_user_email != ''){
      $.ajax({
          url: 'index.php/User/register_js', 
          method: 'POST',
          dataType:'JSON',
          data: {
            user_acc:r_user_acc,
            user_pwd:r_user_pwd,
            user_name:r_user_name,
            user_email:r_user_email,
          }, 
          success: function(response) {
            console.log(response[0]);
            switch(response[0]){
              case '0' :
                  Swal.fire('註冊失敗', response[1], 'error');
                break;
              case '1' :
                // 先關閉註冊modal
                $('#register_modal').modal('hide')
                // Alert
                Swal.fire({
                    title: '註冊成功',
                    text: r_user_acc,
                    icon: 'success',
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: true,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    didClose: () => {
                      // 重新載入
                      window.location.reload();
                    }
                  });
                break;
            }
          },
          error: function(xhr, status, error) {
            // 處理 AJAX 失敗的回應
            console.error(xhr);
            Swal.fire('失敗', '', 'error');
          }
        });
    }
    else
    {
      Swal.fire('請填寫完整資料','','error');
    }
  }

</script>