<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<!DOCTYPE html>
<html>
<head>
    <title>CodeIgniterAppTest</title>
    <!-- 其他樣式和引入 -->
</head>
<body>
    <div id="menu">
        <!-- menu內容 -->
        <?php $this->load->view('layouts/menu'); ?>
    </div>

    <div id="content">
        <!-- 內容區塊 -->
        <?php $this->load->view($content_view); ?>
    </div>

    <div id="footer">
        <!-- footer內容 -->
        <?php $this->load->view('layouts/footer'); ?>
    </div>
</body>
</html>