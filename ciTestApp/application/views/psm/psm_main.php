<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>CI_Framework</title>

	<style type="text/css">

		::selection { background-color: #E13300; color: white; }
		::-moz-selection { background-color: #E13300; color: white; }

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: #003399;
			background-color: transparent;
			font-weight: normal;
		}

		h1 {
			color: #444;
			background-color: transparent;
			border-bottom: 1px solid #D0D0D0;
			font-size: 19px;
			font-weight: normal;
			margin: 0 0 14px 0;
			padding: 14px 15px 10px 15px;
		}

		code {
			font-family: Consolas, Monaco, Courier New, Courier, monospace;
			font-size: 12px;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 14px 0 14px 0;
			padding: 12px 10px 12px 10px;
		}

		#body {
			margin: 0 15px 0 15px;
		}

		p.footer {
			text-align: right;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
		.fadeinDiv
		{
			opacity:0;
		}
		.card-img-top {
			height: 250px; 
			object-fit: cover; 
		}

	</style>
</head>
<!-- 引入 Highcharts 的 JavaScript 文件 -->
 <script src="https://code.highcharts.com/highcharts.js"></script>
<body>
<!-- 資料上傳 -->
<div class="card">
	<div class="card-body">
		<h5 class="card-title"></h5>
		<form>
			<p class="card-text"><small class="text-muted">SheetJS</small></p>
			<!-- SheetJs 上傳 -->
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="import_type" id="radioTest" value="test" checked>
				<label class="form-check-label" for="radioTest">測試</label>
			</div>
			<div class="form-check form-check-inline">
				<input class="form-check-input" type="radio" name="import_type" id="radioPSM" value="psm">
				<label class="form-check-label" for="radioPSM">PSM資料上傳</label>
			</div>
			<div class="input-group mb-3">
				<div class="custom-file">
					<input type="file" class="custom-file-input" id="excelFile" accept=".xlsx, .xls" onchange="display_file_name(this)">
					<label class="custom-file-label" for="excelFile">選擇檔案</label>
				</div>
				<div class="input-group-append">
					<button type="button" class="btn btn-sm btn-outline-secondary" onclick="upload_excel()">上傳</button>
				</div>
			</div>
			<hr>
			<!--  -->
			<p class="card-text"><small class="text-muted">績效分析</small></p>
			<div class="mb-3">
				<label for="select_month" class="form-label">選擇年月：</label>
				<input type="month" class="form-control form-control-sm" id="select_month">
			</div>
			<button type="button" class="btn btn-outline-primary btn-sm" onclick="create_chart()">產生圖表</button>
		</form>
	</div>
</div>
<hr>
<!-- chartContainer -->
<div class="card">
    <div id="chartContainer" class="highcharts-container"></div>
</div>
	<!-- 測試用按鈕 -->
	<div class="fixed-bottom">
		<button type="button" class="btn btn-outline-light" data-toggle="modal" data-target="#test_modal">
			_test_js
		</button>
	</div>
	<!-- 測試用modal -->
	<div class="modal fade" id="test_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">_test_js</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5 class="card-title"></h5>
					<form>
						<hr>
						<!--  -->
						<p class="card-text"><small class="text-muted">OPEN AI</small></p>
						<div class="mb-3">
							<input type="text" class="form-control form-control-sm" id="openai_apiKey" placeholder="API Key">
						</div>
						<div class="mb-3">
							<input type="text" class="form-control form-control-sm" id="openai_question" placeholder="輸入你的問題">
						</div>
						<div class="d-flex justify-content-end">
							<button type="button" class="btn btn-outline-primary btn-sm" onclick="openAI_ask()">送出</button>
						</div>
						<hr>
						<!--  -->
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
$(document).ready(function() {
	// 滾輪滑動div fadein效果 hideme
    $(window).scroll( function(){
    
        /* Check the location of each desired element */
        $('.fadeinDiv').each( function(i){
            
            var bottom_of_object = $(this).offset().top + $(this).outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
            /* If the object is completely visible in the window, fade it it */
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},500);
                    
            }
            
        }); 
    
    });
});
// SheetJS 上傳檔案
function upload_excel(){
	// 先出現處理畫面
	Swal.fire({
		title: '資料讀取中...',
		allowOutsideClick: false,
	});
	const input = document.getElementById('excelFile');
	// import_type
	const import_type = document.querySelector('input[name="import_type"]:checked').value;
    
    const file = input.files[0];
	// 如果有檔案
    if (file) {
        const reader = new FileReader();
        reader.onload = function (e) {
            const data = e.target.result;
            const workbook = XLSX.read(data, { type: 'binary' });
            // 選擇第一個工作表
            const worksheet = workbook.Sheets[workbook.SheetNames[0]];
            // 將工作表轉換為 JSON
            const jsonData = XLSX.utils.sheet_to_json(worksheet, { header: 1 });
            // 進行處理
			switch(import_type){
				case 'test':
					console.log(jsonData);
					const data_length = jsonData.length;
					Swal.fire('讀取成功', '資料長度 : ' + data_length , 'success');
					break;
				case 'psm':
					Swal.fire({
					title: '請稍候...',
					text: 'PSM資料正在處理中',
					// allowOutsideClick: false,
					showConfirmButton: false,
					didOpen:()=>{
						Swal.showLoading();
						// 執行 因為資料量太大所以切分
							let data_count = 0; //切分用
							let total_data = 0; //共計筆數
							let set_data_ary = []; //寫入用
							jsonData.forEach(function(data,index){
								set_data_ary.push(data);
								data_count ++;
								total_data ++;
								if(data_count >= 30 || index == jsonData.length - 1){
									// AJAX
									$.ajax({
									url: 'Psm/import_excel_sheetJs', 
									method: 'POST',
									dataType:'JSON',
									data:{
										session_user_acc:sessionStorage['user_acc'],
										jsonData:set_data_ary,
									},
									success: function(response) {
										// 回應
										// console.log("AJAX")
										// console.log(response)
									},
									error: function(xhr, status, error) {
											// 處理 AJAX 失敗的回應
											console.error(xhr);
											Swal.fire('失敗', '', 'error');
										}
									});
									// 清空
									data_count = 0;
									set_data_ary = [];
								}
							})
							// 執行結束
							Swal.fire('上傳成功', '共計 ' + total_data + ' 筆資料', 'success');
						}
						
					});
					break;
			}
        };
        reader.readAsBinaryString(file);
    }
	else
	{
		console.log("讀取失敗!");
		Swal.fire('失敗', '尚未選擇檔案', 'error');
	}
}
// SheetJS 預覽選擇檔案
function display_file_name(input){
		const file_Name = input.files[0].name;
        const label = input.nextElementSibling;
        label.innerText = file_Name;
}
// 產生圖表
function create_chart(){
	const select_month = document.getElementById('select_month').value;
	console.log(select_month)
	if(select_month != ''){
		Swal.fire({
			title: '請稍候...',
			text: 'PSM資料正在處理中',
			// allowOutsideClick: false,
			showConfirmButton: false,
			didOpen:()=>{
				Swal.showLoading();
				// AJAX
				$.ajax({
					url: 'Psm/export_data_to_chart', 
					method: 'POST',
					dataType:'JSON',
					data:{
						select_month:select_month,
					},
					success: function(response) {
						// 回應
						// console.log("AJAX")
						// console.log(response)
						// 產生圖表
						if(response[0] == 1){
							Highcharts.chart('chartContainer', {
								chart: {
									type: 'column'
								},
								title: {
									text: '各區域總計分析'
								},
								xAxis: {
									categories: Object.keys(response[1]) //keys
								},
								yAxis: {
									title: {
										text: '總計營業額'
									}
								},
								series: [{
									name: '區域',
									colors: [
										'#9b20d9', '#9215ac', '#861ec9', '#7a17e6', '#7010f9', '#691af3',
										'#6225ed', '#5b30e7', '#533be1', '#4c46db', '#4551d5', '#3e5ccf',
										'#3667c9', '#2f72c3', '#277dbd', '#1f88b7', '#1693b1', '#0a9eaa',
										'#03c69b',  '#00f194'
									],
									colorByPoint: true,
									groupPadding: 0,
									data: Object.values(response[1]) //values
								}],
								accessibility: {
									enabled: false // 隱藏無障礙性模塊的警告
								}
							});
							Swal.fire('成功', '', 'success');
						}
						else
						{
							console.log(response);
							Swal.fire('失敗', '查無資料', 'error');
						}
					},
					error: function(xhr, status, error) {
							// 處理 AJAX 失敗的回應
							console.error(xhr);
							Swal.fire('失敗', '', 'error');
						}
				});
			}
		})		
	}
	else{
		Swal.fire('失敗', '尚未選擇日期', 'error');
	}
}
</script>