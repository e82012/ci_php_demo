<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller {
	public function __construct() {
        parent::__construct();
        // 載入資料庫類
        $this->load->database();
		// 載入model
		$this->load->model('Tbl_goods_model');
		$this->load->model('Tbl_note_model');
		$this->load->model('tbl_alert_log_model');
		$this->load->model('Tbl_user_model');
		// header板模
		// 
		$this->load->library('session');

    }

	public function index()
	{
		$seesionData = isset($_SESSION)?$_SESSION:'';
		// 檢查是提醒事項
		$note_ary = array();
		if(isset($seesionData['user_acc'])){
			$user_acc = $seesionData['user_acc'];
			$note_ary = $this->Tbl_note_model->get_note_by_user_acc($user_acc);
		}

		// 產品row carousel用
		$all_goods = $this->Tbl_goods_model->get_all_goods();


		// viewAry
		$viewAry = array(
			'all_goods'=>$all_goods,
			'seesionData'=>$seesionData,
			'note_ary'=>$note_ary,
		);
		$this->load->view('layouts/main_header',$viewAry);
		$this->load->view('welcome_message',$viewAry);
		// $this->smarty->display('welcome_message.tpl');
	}
	// 新增提醒js
	public function create_note_js(){
		$result_Ary = array();
		$cdate = date("Y-m-d h:m:s");
		// 資料
		$session_user_acc = isset($_POST['session_user_acc'])?$_POST['session_user_acc']:'';
		$note_title = isset($_POST['note_title'])?$_POST['note_title']:'';
		$note_email_opt = isset($_POST['note_email_opt'])?$_POST['note_email_opt']:'0';
		$note_line_opt = isset($_POST['note_line_opt'])?$_POST['note_line_opt']:'0';
		$note_line_token = isset($_POST['note_line_token'])?$_POST['note_line_token']:'';
		$note_alert_date = isset($_POST['note_alert_date'])?$_POST['note_alert_date']:null;
		$note_content = isset($_POST['note_content'])?$_POST['note_content']:'';
		$note_cdate = $cdate;
		// 建立
		if($session_user_acc != '' && $note_title != '' && $note_content != ''){
			$note_Data = array(
				'note_user_acc'=>$session_user_acc,
				'note_title'=>$note_title,
				'note_email_opt'=>$note_email_opt,
				'note_line_opt'=>$note_line_opt,
				'note_line_token'=>$note_line_token,
				'note_alert_date'=>$note_alert_date,
				'note_alert_opt'=>'0',
				'note_content'=>$note_content,
				'note_cdate'=>$note_cdate,
				'note_status'=>'0',
				'opt1'=>'1',
			);
			$this->Tbl_note_model->insert_note($note_Data);
			array_push($result_Ary,"1","新增成功");
		}
		else{
			array_push($result_Ary,"0","新增失敗");
		}


		print json_encode($result_Ary);

	}
	// 修改提醒js
	public function update_note_js(){
		$result_Ary = array();
		$udate = date("Y-m-d h:m:s");
		// 資料
		$note_id = isset($_POST['note_id'])?$_POST['note_id']:'';
		$session_user_acc = isset($_POST['session_user_acc'])?$_POST['session_user_acc']:'';
		$note_title = isset($_POST['note_title'])?$_POST['note_title']:'';
		$note_email_opt = isset($_POST['note_email_opt'])?$_POST['note_email_opt']:'0';
		$note_line_opt = isset($_POST['note_line_opt'])?$_POST['note_line_opt']:'0';
		$note_line_token = isset($_POST['note_line_token'])?$_POST['note_line_token']:'';
		$note_alert_date = isset($_POST['note_alert_date'])?$_POST['note_alert_date']:null;
		$note_content = isset($_POST['note_content'])?$_POST['note_content']:'';
		$note_udate = $udate;
		// 建立
		if($note_id!= ''&& $session_user_acc != '' && $note_title != '' && $note_content != ''){
			$note_Data = array(
				'note_user_acc'=>$session_user_acc,
				'note_title'=>$note_title,
				'note_email_opt'=>$note_email_opt,
				'note_line_opt'=>$note_line_opt,
				'note_line_token'=>$note_line_token,
				'note_alert_date'=>$note_alert_date,
				'note_alert_opt'=>'0', //有修改過重新發送通知
				'note_content'=>$note_content,
				'note_udate'=>$note_udate,
			);
			$this->Tbl_note_model->update_note($note_id,$note_Data);
			array_push($result_Ary,"1","更新成功");
		}
		else{
			array_push($result_Ary,"0","更新失敗");
		}


		print json_encode($result_Ary);
	}
	// 完成提醒js
	public function completed_note_js(){
		$result_Ary = array();
		// 資料
		$note_id = isset($_POST['note_id'])?$_POST['note_id']:'';
		$session_user_acc = isset($_POST['session_user_acc'])?$_POST['session_user_acc']:'';
		$note_title = isset($_POST['note_title'])?$_POST['note_title']:'';
		$note_content = isset($_POST['note_content'])?$_POST['note_content']:'';
		// 更新完成
		if($note_id!= ''&& $session_user_acc != '' && $note_title != '' && $note_content != ''){
			$this->Tbl_note_model->update_note_completed($note_id);
			array_push($result_Ary,"1","更新成功");
		}
		else{
			array_push($result_Ary,"0","更新失敗");
		}

		print json_encode($result_Ary);
	}
	// 刪除提醒js
	public function delete_note_js(){
		$result_Ary = array();
		// 資料
		$note_id = isset($_POST['note_id'])?$_POST['note_id']:'';
		$session_user_acc = isset($_POST['session_user_acc'])?$_POST['session_user_acc']:'';
		$note_title = isset($_POST['note_title'])?$_POST['note_title']:'';
		$note_content = isset($_POST['note_content'])?$_POST['note_content']:'';
		// 刪除資料
		if($note_id!= ''&& $session_user_acc != '' && $note_title != '' && $note_content != ''){
			$this->Tbl_note_model->delete_note($note_id);
			array_push($result_Ary,"1","刪除成功");
		}
		else{
			array_push($result_Ary,"0","刪除失敗");
		}
		print json_encode($result_Ary);
	}


}
