<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Cron_job extends CI_Controller {
	public function __construct() {
        parent::__construct();
        // 載入資料庫類
        $this->load->database();
		// 載入model
		$this->load->model('Tbl_user_model');
		$this->load->model('Tbl_note_model');
		$this->load->model('Tbl_alert_log_model');
		// 
    }

	public function index()
	{
		
		return;
	}
	// 發送提醒事項通知___Cron_job/send_alert
	public function send_alert(){
		$start_time = new DateTime();
		echo "[Begin]*********************************";
		echo "send_alert";
		echo "程式-開始時間:{$start_time->format('Y-m-d H:i:s')}";

		// 處理件數
		$alert_num = 0;
		// 檢查時間
		$chk_date = date("Y-m-d H:m");
		// $chk_date = '2023-12-11 17:10';
		// 所有未完成需發送通知的提醒事項
		$alert_data = $this->Tbl_note_model->_get_all_alert_note();
		foreach($alert_data as $data){
			$alert_date = date("Y-m-d H:i", strtotime($data->note_alert_date));
			// 檢查項目通知時間
			if($alert_date == $chk_date){
				$alert_id = $data->id;
				$alert_user = $data->note_user_acc;
				$alert_title = $data->note_title;
				$alert_content = $data->note_content;
				$alert_msg = $alert_user.'\n 提醒通知 : '.$alert_title . '\n 內容 : ' . $alert_content;
				$alert_json = json_encode(array($alert_user,$alert_title,$alert_content));
				// 進行傳送 email、line
				if($data->note_email_opt == 1){
					// 取得email
					$user_email = $this->Tbl_user_model->get_email_by_user_acc($data->note_user_acc);
					$this->send_email($user_email,$alert_title,$alert_content);
				}
				if($data->note_line_opt == 1){
					$line_token = $data->note_line_token;
					$this->send_line_notify($line_token,$alert_msg);
				}
				// 進行提醒狀態更新、寫入log
				$update_note_data = array(
					'note_alert_opt'=>'1',
				);
				// tbl_note 更新為已發送
				$this->Tbl_note_model->update_note($alert_id,$update_note_data);
				// 寫入log 
				$send_date = date("Y-m-d h:m:s");
				$log_data = array(
					'alert_type'=>'tbl_note',
					'alert_id'=>$alert_id,
					'alert_date'=>$send_date,
					'alert_content'=>$alert_json,
					'opt1'=>'1'
				);
				// tbl_alert_log 寫入
				$this->Tbl_alert_log_model->insert_log($log_data);
				
				// 處理件數++
				$alert_num ++;
			}
		}
		$end_time = new DateTime();
		$diff_time = $end_time->diff($start_time);
		echo "程式-結束時間:{$end_time->format('Y-m-d H:i:s')}";
		echo "程式-處理時間:{$diff_time->h}時 {$diff_time->i}分 {$diff_time->s}秒";
		echo "檢查時間:".$chk_date." 已處理 ".$alert_num." 件通知";
		echo "[End]********************************";

		return;
	}
	// 發送email
	public function send_email($email,$title,$content){
		// $mail = new phpmailer();
        // try {
        //     $mail->SMTPDebug = 1;
        //     // 戰國策
        //     $mail->isSMTP();  //設定使用SMTP方式寄信
        //     $mail->SMTPAuth = TRUE;   //設定SMTP需要驗證			
        //     $mail->Host = "smtp.gmail.com";  //GCP的SMTP主機
        //     $mail->Port = 587;    //智邦的SMTP主機的SMTP埠位為465埠
        //     $mail->SMTPSecure = "tls";   // 智邦的SMTP主機需要使用SSL連線
        //     $mail->CharSet = "utf-8";   //設定郵件編碼，預設UTF-8
        //     $mail->Username = "";  //帳號
        //     $mail->Password = "";  //密碼
        //     $mail->setFrom('notify@jithouse.com', 'email');

        //     $mail->FromName = "CI_Test_Cron_Job";   //設定寄件者姓名
        //     $mail->Subject =$title;    //設定郵件標題

        //     $mail->Body = $content;
        //     $mail->IsHTML(true); //設定郵件內容為HTML       
        //     $mail->AddAddress($email);   
                    
        //     if (!$mail->Send()) {
        //         var_dump($mail,10,true);
        //     }
        // } catch (phpmailerException $e) {
        //     $errors[] = $e->errorMessage(); //Pretty error messages from PHPMailer
        //     var_dump($e,10,true);
        // } catch (Exception $e) {
        //     $errors[] = $e->getMessage(); //Boring error messages from anything else!
        //     var_dump($e,10,true);
        // }            
            
		return 'OK';
	}
	// 發送lineNotify
	public function send_line_notify($token,$msg){
		// $headers = array(
        //     'Content-Type: application/x-www-form-urlencoded',
        //     'Authorization:Bearer '.$token
        // );
        // $message = array(
        //     'message' => $msg,
        // );
        // $ch = curl_init();
        // curl_setopt($ch , CURLOPT_URL , "https://notify-api.line.me/api/notify");
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($message));
        // $result = curl_exec($ch);
        // curl_close($ch);

		return 'OK';
	}

}
