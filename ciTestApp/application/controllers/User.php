<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct() {
        parent::__construct();
        // 載入資料庫類
        $this->load->database();
		// 載入model
		$this->load->model('tbl_user_model');
		// 
		$this->load->library('session');

    }

	public function index()
	{
		
		return;
	}
	// 登入
	public function login_Js(){
		$result_Ary = array();
		$user_acc = isset($_POST['user_acc'])?$_POST['user_acc']:'';
		$user_pwd = isset($_POST['user_pwd'])?$_POST['user_pwd']:'';

		// 檢查使用者帳號
		$user_data = $this->tbl_user_model->get_user_by_user_acc($user_acc);
		// // 檢查密碼正確
		if(!empty($user_data)){
			if(password_verify($user_pwd,$user_data[0]->user_pwd)){
				// 登入成功，寫入SEESION
				$_SESSION['user_acc'] = $user_data[0]->user_acc;
				$_SESSION['user_name'] = $user_data[0]->user_name;
				array_push($result_Ary,"1",$user_acc);
			}
			else
				array_push($result_Ary,"0","帳號或密碼錯誤");
		}
		else
			array_push($result_Ary,"0","資料不存在");

		print json_encode($result_Ary);
        // exit;
	}
	// 登出
	public function logout_js(){
		$result_Ary = '1';
		// 清除SEESION
		session_destroy();
		print json_encode($result_Ary);
	}
	// 註冊
	public function register_js(){
		$result_Ary =array();
		// 註冊資料
		$user_acc = isset($_POST['user_acc'])?$_POST['user_acc']:'';
		$user_pwd = isset($_POST['user_pwd'])?$_POST['user_pwd']:'';
		$user_name = isset($_POST['user_name'])?$_POST['user_name']:'';
		$user_email = isset($_POST['user_email'])?$_POST['user_email']:'';
		
		//先檢查有無重複帳號
		$chk_acc = $this->tbl_user_model->get_user_by_user_acc($user_acc); 
		if(empty($chk_acc)){
			$cdate = date("Y-m-d h:m:s");
			$resData = array(
				'user_acc'=>$user_acc,
				'user_pwd'=>password_hash($user_pwd, PASSWORD_BCRYPT),
				'user_name'=>$user_name,
				'user_email'=>$user_email,
				'user_cdate'=>$cdate,
				'opt1'=>'1'
			);
			$this->tbl_user_model->insert_user($resData);
			array_push($result_Ary,"1","註冊成功");
		}
		else{
			array_push($result_Ary,"0","帳號已使用");
		}

	
		print json_encode($result_Ary);
	}
}
