<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Psm extends CI_Controller {
	public function __construct() {
        parent::__construct();
        // 載入資料庫類
        $this->load->database();
		// 載入model
		$this->load->model('tbl_user_model');
		$this->load->model('Tbl_psm_log_model');
		// 
		$this->load->library('session');

    }

	public function index()
	{
		$viewAry = array();
		
		$this->load->view('layouts/main_header',$viewAry);
		$this->load->view('psm/psm_main',$viewAry);
	}
	// sheetJs寫入資料庫
	public function import_excel_sheetJs(){
		$result_Ary = array();
		$import_count = 0;
		// 資料讀入
		$jsonData = isset($_POST['jsonData'])?$_POST['jsonData']:'';
		$session_user_acc = isset($_POST['session_user_acc'])?$_POST['session_user_acc']:'';
		$test_ary = array();
		foreach($jsonData as $data){
			// 寫入資料
			$insert_data = $this->psm_insert_json_to_col($data,$session_user_acc);
			$this->Tbl_psm_log_model->insert_data($insert_data);
			$import_count ++;
			array_push($test_ary,$data[6]);
		}
		array_push($result_Ary,"1",$import_count);

		print json_encode($result_Ary);
	}
	// psm 將json每筆資料放入欄位 再回傳寫入
	public function psm_insert_json_to_col($data,$c_user){
		$c_date = date("Y-m-d h:m:s");
		$insert_data_ary = array(
			'date_year' => $data['0'],
			'date_month' => $data['1'],
			'dept' => $data['2'],
			'divi' => $data['3'],
			'area' => $data['4'],
			'store_type' => $data['5'],
			'store_name' => $data['6'],
			'serv_1' => $data['7'],
			'serv_2' => $data['8'],
			'serv_3' => $data['9'],
			'serv_4' => $data['10'],
			'serv_5' => $data['11'],
			'serv_6' => $data['12'],
			'serv_7' => $data['13'],
			'serv_8' => $data['14'],
			'serv_9' => $data['15'],
			'serv_10' => $data['16'],
			'serv_11' => $data['17'],
			'serv_12' => $data['18'],
			'serv_13' => $data['19'],
			'serv_14' => $data['20'],
			'serv_15' => $data['21'],
			'serv_16' => $data['22'],
			'serv_17' => $data['23'],
			'income' => $data['24'],
			'c_date'=>$c_date,
			'c_user'=>$c_user,
			'opt1'=>1
		);

		return $insert_data_ary;
	}
	// 取得圖表所需資料
	public function export_data_to_chart(){
		$result_Ary = array();
		$select_month = isset($_POST['select_month'])?$_POST['select_month']:'';
		// 如果有選擇日期資料才進行查詢
		if($select_month != ''){
			// 分割拿出年月
			$new_select_month = explode('-',$select_month);
			$s_year = $new_select_month[0];
			$s_month = ltrim($new_select_month[1],0);
			// 資料查詢
			$psm_data = $this->Tbl_psm_log_model->get_data_by_ym($s_year,$s_month);
			// 進行資料整理
			if(!empty($psm_data)){
				$psm_result = array();
				foreach($psm_data as $psm){
					// array_push($psm_result,$psm->income);
					if(!isset($psm_result[$psm->area]))
						$psm_result[$psm->area] = $psm->income;
					else
						$psm_result[$psm->area] += $psm->income;
				}
				unset($psm_result['全公司']); //移除
				array_push($result_Ary,"1",$psm_result);

			}
			else{
				array_push($result_Ary,"0","查無資料");
			}

		}

		print json_encode($result_Ary);
	}
	
}
