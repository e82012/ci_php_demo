<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$active_group = 'default';
	$query_builder = TRUE;

	$db['default'] = array(
		'dsn'      => '',
		// hostname改為image名稱
		'hostname' => 'cidocker73-mariadb-1', 
		'username' => 'root',
		'password' => '123456',
		'database' => 'database',
		'dbdriver' => 'mysqli',
		'dbprefix' => '',
		'pconnect' => FALSE,
		'db_debug' => (ENVIRONMENT !== 'production'),
		'cache_on' => FALSE,
		'cachedir' => '',
		'char_set' => 'utf8',
		'dbcollat' => 'utf8_general_ci',
		'swap_pre' => '',
		'encrypt'  => FALSE,
		'compress' => FALSE,
		'stricton' => FALSE,
		'failover' => array(),
		'save_queries' => TRUE
	);
?>