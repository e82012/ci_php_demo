# 使用官方 PHP 映像
FROM php:7.3-apache

# 安裝必要套件
RUN apt-get update && apt-get install -y git zip unzip libzip-dev

# 安裝 Composer
COPY --from=composer /usr/bin/composer /usr/bin/composer

# 將專案複製到容器中
COPY ./ciTestApp /var/www/html

# 在專案目錄執行 composer install
WORKDIR /var/www/html
RUN composer install --no-plugins --no-scripts --no-interaction

# 安裝 MySQLi 擴展
RUN docker-php-ext-install mysqli

# 安裝 unzip 工具
RUN apt-get update && apt-get install -y unzip

# 安裝 gd 和 zip 擴展(PhpOffice/PhpSpreadsheet會需要用到)
# RUN apt-get install -y libpng-dev libjpeg-dev libfreetype6-dev libzip-dev
# RUN docker-php-ext-configure gd --with-jpeg --with-freetype
# RUN docker-php-ext-install gd zip

# 安裝 PhpOffice/PhpSpreadsheet
# WORKDIR /var/www/html
# RUN composer require phpoffice/phpspreadsheet

# 安裝 Smarty 版本 4.1.1
# RUN composer require smarty/smarty:^4.1

# 安裝 CKEditor 版本 4.19.0
# RUN composer require ckeditor/ckeditor:^4.19

# 安裝 RESPONSIVE filemanager 版本 9.14.0
# COPY ./ciTestApp/vendor/responsive_filemanager /var/www/html/vendor/responsive_filemanager

# 啟用 Apache Rewrite 模組
RUN a2enmod rewrite

# 重新啟動 Apache
RUN service apache2 restart