- PHP 7.3 apache + MariaDB  
- Framework : CodeIgniter 3.1.13
- JQuery、Bootstrap4、Sweetalert2、SheetJs、Highcharts
- Docker Container(PHP、myphpadmin、mariaDB)
> ---
**CI框架**  
1. Carousel輪播圖、4格Card輪播  
2. div滾動載入    
3. 簡易會員系統(註冊、登入、登出)
4. 提醒事項清單(CRUD)
5. 提醒事項自動發送通知(Cron_job/send_alert)  
6. SheetJs資料匯入  (psm)
7. HighCharts圖表產生  (psm)
> ---
 - 頁面內容
 0. menu
 1. 輪播圖
 2. 4格輪播
 3. 提醒事項
 

